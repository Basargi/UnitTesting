﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace UnitTesting
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            LblPrice.Text = (Convert.ToDecimal(EntryA.Text) + Convert.ToDecimal(EntryB.Text)).ToString();
        }

        private void EntryA_TextChanged(object sender, TextChangedEventArgs e)
        {
            EntryB.Text = (((Convert.ToDecimal(EntryA.Text) * Convert.ToDecimal(18)) / 100)).ToString();
        }
    }
}
