﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UnitTest
{
    [TestFixture(Platform.Android)]
   // [TestFixture(Platform.iOS)]
    public class Tests
    {
        IApp app;
        Platform platform;

        public Tests(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform);
        }

        [Test]
        public void AppLaunches()
        {
            app.Screenshot("First screen.");
        }
        [Test]
        public void CheckGSTTax()
        {
            //Arrange
            app.EnterText("EntryPrice", "100");
            app.EnterText("EntryGST", "18");

            //Act
            app.Tap("SubmitButton");

            //Assert
            var EA=app.Query("EntryPrice").First().Text;
            var EB = app.Query("EntryGST").First().Text;
            string totalPrice= (Convert.ToDecimal(EA) + (((Convert.ToDecimal(EA) * Convert.ToDecimal(EB)) / 100))).ToString();
            var result=app.Query("TotalPrice").First(c=>c.Text==totalPrice);
            Assert.IsTrue(result != null, "GST is not correct");
        }
    }
}

